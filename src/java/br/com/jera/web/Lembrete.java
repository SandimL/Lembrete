/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jera.web;

import br.com.jera.dao.LivroDAO;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sandim
 */
@WebServlet(urlPatterns="/lembrete")
public class Lembrete extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        
        /*Relaciona o livo ao lembrete.
          Salva o lembrete.*/
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        long id = Integer.valueOf(req.getParameter("id"));
        Date lembrete = new Date();
        LivroDAO livroDAO = new LivroDAO();
        livroDAO.atualizaLivro(id, lembrete);
        resp.sendRedirect("paginainicial.html");
        //
        
    }

    
}
