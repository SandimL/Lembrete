/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jera.dao;

import br.com.jera.gerenciador.Livro;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sandim
 */
public class LivroDAO 
{
    
    //Cria um metodo de hash para simular o BDD.
    private final static Map<Long, Livro> LIVRO = new HashMap<>();
    //
    
    public void adicionarLivro(Livro livro)
    {
        geraIdEAdiciona(livro);
    }
    
    public Collection<Livro>buscaLivro()
    {
        return LIVRO.values();
    }
    
    private static void geraIdEAdiciona(Livro livro) 
    {
        /*gera o ID do proximo livro a ser adicionado
          de acordo com o tamanho da Collection*/
        
        long id = LIVRO.size()+1;
        livro.setId(id);
        LIVRO.put(id, livro);  
    }
    
    //relaciona o livro com o lembrete atravéz do ID
    public static void atualizaLivro(long id, Date lembrete)  
    {  
        Livro livro = LIVRO.get(id);
        livro.setLembrete(lembrete);
        LIVRO.replace(id, livro);
    }
    //
    
}
